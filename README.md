It's recommended that you use a crontab for this if you're wanting to record how many ads were blocked on a specific day (dump.sh).

Example:

0 0 * * * /root/dump.sh

0 0 * * * kill -s SIGTERM $(pgrep -f "/bin/ash /root/adbcount.sh") && /root/adbcount.sh

To autostart this script on reboot, in /etc/rc.local (@reboot crontab doesn't work on OpenWRT): /root/adbcount.sh

But of course, your use case could be different.

Dnsmasq logging needs to be turned on for this to work. Don't worry, this doesn't write to flash memory by default. It gets logged to OpenWRT's logd service, which uses a temporary buffer for storage.

Add option logqueries '1' to /etc/config/dhcp under the 'dnsmasq' entry to turn it on.


